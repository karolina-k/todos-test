describe('TodoList Test', () => {

 let todoItemOne = 'have a cup of coffee'
 let todoItemTwo = 'feed the cat'
 let todoItemThree = 'buy some milk'

  beforeEach(() => {
    cy.visit('/');
  })

  context('When the page is opened for the first time', () => {
    it('should be focused on to-do input field', () => {
      cy.focused().should('have.class', 'new-todo')
    })
  })

  context('Nothing to do', () => {
    it('should display only to-do input field', () => {
      cy.get('.new-todo')
        .should('be.visible')
      
      cy.get('.main') //display: none
        .should('not.be.visible')

      cy.get('.footer')
        .should('not.be.visible')
    })
  })

  context('New to-do items', () => {
    it('should add new items to the list', () => {
      cy.get('.new-todo')
        .insertTodos()

      cy.get('.todo-list li')
        .should('have.length', 3)
    })

    it('should clear input field when item is added', () => {
      cy.get('.new-todo')
        .type(todoItemOne)
        .type('{enter}')

      cy.get('.new-todo')
        .should('be.empty');
    })

    it('should add new item in the bottom of the list', () => {
      cy.get('.new-todo') // subject  ($input from command)
        .insertTodos()

      cy.get('.todo-list li')
        .eq(0)
        .should('contain', todoItemOne);

      cy.get('.todo-list li')
        .eq(1)
        .should('contain', todoItemTwo);

      cy.get('.todo-list li')
        .eq(2)
        .should('contain', todoItemThree);
    })

    it('should trim text input', () => { 
      cy.get('.new-todo')
        .type(` ${todoItemOne}  `)
        .type('{enter}');

      cy.get('.todo-list li')
        .eq(0)
        .should('have.text', todoItemOne);
    })

    it('should display #footer and #main', () => {
      cy.get('.new-todo')
        .insertTodos(1);
      
      cy.get('.main') 
        .should('be.visible');

      cy.get('.footer')
        .should('be.visible');
    })
  })

  context('Mark items', () => {
    beforeEach(() => {
      cy.get('.new-todo')
        .insertTodos()
    })

    it('should check item as completed', () => {
      cy.get('.toggle-all')
        .check();

      cy.get('.todo-list li')
        .eq(0)
        .should('have.class', 'completed');

      cy.get('.todo-list li')
        .eq(1)
        .should('have.class', 'completed');

      cy.get('.todo-list li')
        .eq(2)
        .should('have.class', 'completed');
    })

    it('should uncheck completed items', () => {
      cy.get('.toggle-all')
        .check()
        .uncheck();

      cy.get('.todo-list li')
        .eq(0)
        .should('not.have.class', 'completed');

      cy.get('.todo-list li')
        .eq(1)
        .should('not.have.class', 'completed');

      cy.get('.todo-list li')
        .eq(2)
        .should('not.have.class', 'completed');
  })
  })

  context('Edit', () => {
    beforeEach(() => {
      cy.get('.new-todo')
        .insertTodos()    
    })

    it('should edit to-do item', () => {
      cy.get('.todo-list li')
        .eq(0)
        .find('label')
        .dblclick()

      cy.get('.edit')
        .eq(0)
        .clear()
        .type('new one to do')
        .type('{enter}')

      cy.get('.todo-list li')
        .eq(0)
        .should('not.have.text', todoItemOne)
    })

    it('should edit item on blur', () => {
      cy.get('.todo-list li')
        .eq(0)
        .find('label')
        .dblclick()
        
      cy.get('.edit')
        .eq(0)
        .type('!!!')
        .blur();

      cy.get('.todo-list li')
        .eq(0)
        .should('have.text', (`${todoItemOne}!!!`)) 
    })
    it('should cancel edit on escape', () => {
      cy.get('.todo-list li:first')
        .find('label')
        .dblclick()
      
      cy.get('.edit:visible')
        .type('- try to edit')
        .type('{esc}')

      cy.get('.todo-list li :first')
        .should('have.text', todoItemOne)
    })
  })

  context('Delete', () => {
    it('should delete item from the list', () => {
      cy.get('.new-todo')
        .insertTodos();

      cy.get('.todo-list li')
        .first()
        .find('.destroy')
        .invoke('show')
        .click();

      cy.get('.todo-list li')
        .should('have.length', 2);
    })
  })

  context('Active todos counter', () => {
    beforeEach(() => {
      cy.get('.new-todo')
        .insertTodos();
    })
    it('should display the correct number of active items', () => {
      cy.get('.todo-count')
        .should('contain', '3 items left');
    })

    it('should display the correct number when all items are competed', () => {
      cy.get('.toggle-all')
        .check();

      cy.get('.todo-count')
        .should('contain', '0 items left');
    })

    it('allow to use clear completed button', () => {
      cy.get('.view .toggle:first')
      .check()

      cy.get('.clear-completed')
        .click()

      cy.get('.todo-list li')
        .should('have.length', 2)

      cy.get('.todo-list li:first')
        .should('have.text', todoItemTwo);
    })

    it('should hide clear button when no item is completed', () => {
      cy.get('.clear-completed')
        .should('not.be.visible')
    })
  })
  context('Filters', () => {
    beforeEach(() => {
      cy.get('.new-todo')
        .insertTodos();
    })

    it('should display all items', () => {
      cy.get('.view .toggle:first')
        .check()

      cy.get('.todo-list li')
        .should('have.length', 3)

      cy.get('[data-reactid=".0.2.1.0"] a')
        .should('have.class', 'selected')
    })

    it('should display active items', () => {
      cy.get('[data-reactid=".0.2.1.2.0"]')
        .click()

      cy.get('.todo-list li')
        .should('have.length', 3)
    })

    it('should display completed items', () => {
      cy.get('.toggle-all')
        .check();

      cy.get('[data-reactid=".0.2.1.4.0"]')
        .click()

      cy.get('.todo-list li')
        .should('have.length', 3)
    })

    it('should allow to use back button to change the filter', () => {
      cy.get('[data-reactid=".0.2.1.2.0"]')
        .click()
      
      cy.go('back')

      cy.get('[data-reactid=".0.2.1.0"] a')
        .should('have.class', 'selected')
    })
  })

});
