// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add('insertTodos', { prevSubject: 'element'}, ($newTodoInput, numberOfItems = 3) => {

  let todosToInsert = [
    'have a cup of coffee', 
    'feed the cat',
    'buy some milk'
  ]

  let limitedTodosList = Cypress._.take(todosToInsert, numberOfItems);

  limitedTodosList.forEach((todo) => {

    cy.wrap($newTodoInput)
      .type(todo)
      .type('{enter}')
  })

 })